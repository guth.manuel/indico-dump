from indico_dump.indico import Indico
import os
from indico_dump.logger import logger, set_log_level

set_log_level(logger, "DEBUG")

# # initialise CERN Indico
# indico_cern = Indico(api_token=os.environ["INDICO_API"])

# category = indico_cern.category(category_id=2408, start_date="2022-01-01")

# # FTAG meetings
# category.filter_events(include_list=["FTAG"], exclude_list=["students"])
# category.save_events("FTAGMeetings.json")


# initialise UniGe Indico
indico_unige = Indico(
    api_key=os.environ["INDICO_API_KEY_UNIGE"],
    indico_site="https://partphys-indico.unige.ch",
)

# RODEM weekly physics meetings
category_rodem = indico_unige.category(category_id=19, start_date="2022-01-01")
category_rodem.filter_events(include_list=["RODEM"], exclude_list=["[CANCELLED]"])
category_rodem.save_events("RODEMMeetings.json")

# # tt+charm meeting
# category.filter_events(include_list=["tt+charm", "tt+cc"])
# category.save_events("public/ttccMeetings.json")


# for event in category:
#     event.initlialise_contributions()
#     event.to_dict()
#     print(event.title)
#     print("----")


# category = indico.category(9120, start_date="2021-09-15", end_date="2021-10-15")
# pprint(category.event_list)
# pprint(category.meta_data)
# for event in reversed(category.meta_data["results"]):
#     print(event["id"])
#     print(event["title"])
# print(category.events_df)
# events = category.filter_events(
#     # exclude_list=["ITk"],
#     include_list=["Algorithms"],
# )
# # print(events)
# event = Event(indico)
# event.get(1078661)
# # pprint(event.meta_data)
# # print(event.minutes)
# # pprint(event.to_dict())
# for contr in event:
#     pprint(contr.to_dict())
#     # print(contr.speakers)
#     # print(contr.n_attachements)
#     # print(contr.event.n_contr)
# # contr = Contribution(event)
# # contr.get(0)


# indico = Indico(
#     api_key=os.environ["INDICO_API_KEY_UNIGE"],
#     indico_site="https://partphys-indico.unige.ch",
# )
# event = Event(indico)
# event.get(578, detail="sessions")
# print(event.n_contributions)
# event.initlialise_contributions()
# pprint(event.contributions)
# # pprint(event.meta_data)
# # for contr in event:
# #     print(contr.title)
# #     print(contr.speakers)
# #     print(contr.n_attachements)
# # pprint(contr.session)
# # pprint(contr.to_dict())
